# README #

A function which replicates the wordwrap function in PHP

# PHPUnit tests #

After downloading the repo you will need to run `composer install` inside the downloaded
directory.

When composer has finished you can run the one php test with the following
`vendor/phpunit/phpunit/phpunit tests/WrapTest.php` command from this directory.

Alternatively you can install it globally with the following command
`composer global require "phpunit/phpunit=5.5.*"` and then just run `phpunit tests/WrapTest.php`

### TODO

* Separate index php into a class/function file
* Add the function to a class as a static method
* Confirm the coding convention required
* Review TODO from PHPUnit test
* Maybe add some caching
* Maybe implement some benchmarking
* Look into a single regexp