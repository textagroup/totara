<?php

include_once('wrap.php');

$tests = array(
	array(
		'string' => 'Testing',
		'length' => 10
	),
	array(
		'string' => 'Testing formatting lines',
		'length' => 3
	),
	array(
		'string' => "test1\ntest2",
		'length' => 5
	),
	array(
		'string' => "computer-aided design",
		'length' => 8
	),
	array(
		'string' => "Testing formatting lines",
		'length' => 9
	),
	array(
		'string' => "Testing formatting lines",
		'length' => 3
	)
);

$string = (string) strip_tags($_POST['string']);
$length = (int) $_POST['length'];

if ($string && $length) {
	$tests = array(
		array(
			'string' => $string,
			'length' => $length
		)
	);
}

?>
<html>
	<head>
		<title>Wrap test page</title>
	</head>
	<body>
		<form action = "index.php" method="post">
			<p>String: <br /><textarea name="string" cols=20 rows=10></textarea>
			<p>Length: <input type="text" name="length">
			<p><input type="submit" value="Test"></p>
		</form>
<?php
	foreach ($tests as $test) {
		$result = wrap($test['string'], $test['length']);
		echo '<pre>';
			var_dump($result);
		echo '</pre>';
	}
?>
	</body>
</html>
