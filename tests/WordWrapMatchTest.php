<?php

use PHPUnit\Framework\TestCase;

include_once('wrap.php');

/**
 * Better way of testing by using the smae string and length and parsing 
 * against both the standard wordwrap and the custom wrap function
 */

class WordWrapMatchTest extends TestCase
{
	private $apiCall = 'https://baconipsum.com/api/?type=meat-and-filler';
	private $apiLength = 30;

	public function testWrap() {

		$stringsToTest = array(
			array(
				'string' => 'textagroup@gmail.com',
				'length' => 8
			),
			array(
				'string' => 'computer-aided design',
				'length' => 8
			)
		);

		foreach ($stringsToTest as $test) {
			$string = $test['string'];
			$length = $test['length'];

			$result = wrap($string, $length);
			// need to set cut to true for wordwrap
			$wordWrapResult = wordwrap($string, $length, "\n", true);
			$this->assertEquals($result, $wordWrapResult);
		}
	}

	/**
	 * TODO
	 * Following unit tests proves I am stripping out too much whitespace
	 */
	public function testAPIWrap() {
		$loremIpsumJson = file_get_contents($this->apiCall);
		$loremIpsum = json_decode($loremIpsumJson);

		foreach ($loremIpsum as $string) {
			$result = wrap($string, $this->apiLength);
			$wordWrapResult = wordwrap($string, $this->apiLength, "\n", true);
			$this->assertEquals($result, $wordWrapResult);
		}
	}
}
