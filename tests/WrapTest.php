<?php

use PHPUnit\Framework\TestCase;

include_once('wrap.php');

class WrapTest extends TestCase
{
	public function testWrap() {

		$expected[] = 'Testing';
		$expected[] = "Testing\n" .
			"wrapping\n" .
			"lines";
		$expected[] = "Testing\n" .
			"formatting\n" .
			"lines";
		$expected[] = "Testing\n" .
			"formattin\n" .
			"g lines";
		$expected[] = "Tes\n" .
			"tin\n" .
			"g\n" .
			"for\n" .
			"mat\n" .
			"tin\n" .
			"g\n" .
			"lin\n" .
			"es";
		$expected[] = "Testing-fo\n" .
			"rmatting\n" .
			"lines";
			

		$result = wrap('Testing', 20);
		$this->assertEquals($result, $expected[0]);

		$result = wrap('Testing wrapping lines', 10);
		$this->assertEquals($result, $expected[1]);

		$result = wrap('Testing formatting lines', 10);
		$this->assertEquals($result, $expected[2]);

		$result = wrap('Testing formatting lines', 9);
		$this->assertEquals($result, $expected[3]);

		$result = wrap('Testing formatting lines', 3);
		$this->assertEquals($result, $expected[4]);

		$result = wrap('Testing-formatting lines', 10);
		$this->assertEquals($result, $expected[5]);
	}
}
