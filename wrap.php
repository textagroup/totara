<?php

/**
 * String for wrapping sentences
 *
 * @param string $string the string to format
 * @param int $length the maximum length of the lines
 *
 * @return string the formatted string
 */
function wrap($string ='', $length=0) {
	// type cast the function parameters
	$string = (string)$string;
	$length= (int)$length;

	// can we return early
	$strLength = strlen($string);
	if ($strLength <= $length) {
		return $string;
	}

	// use these to construct the formatted array
	$line = null;
	$lines = array();
	// we do not want to skip adding the last iteration to the array
	$addLastLine = 0;

	// split the string into an array of words
	// may need to add other special characters
	preg_match_all("/[\w\-\.\@]{1,$length}/", $string, $words);


	// now loop through the words array to build the lines array
	$noWords = count($words[0]);
	for ($i=0; $i < $noWords; $i++) {
		$word = $words[0][$i];
		$checkLength = (strlen($line) + strlen($word));
		if ($checkLength < $length) {
			$line = ($line) ? "$line $word" : $word;
			$addLastLine = ($i == ($noWords-1)) ? 1 : 0;
		} else {
			if ($line) {
				$lines[$i] = $line;
				$line = $word;
				$addLastLine = ($i == ($noWords-1)) ? 1 : 0;
			} else {
				$lines[$i] = $word;
			}
		}
	}

	if ($addLastLine == 1) {
		$lines[] = $line;
	}

	// implode the array into a string using a new line as the glue
	return implode("\n", $lines);
}
